const qs = require('querystring')
const Koa = require('koa')
const Router = require('koa-router')
const static = require('koa-static')
const FormData = require('form-data')
const axios = require('axios')

const app = new Koa()
const router = new Router()

app.use(static(__dirname + '/static'))

app.use(async (ctx, next) => {
  try {
    await next()
  } catch(err) {
    if (err.status) {
      ctx.status = err.status
    }

    ctx.body = err.message
  }
})

const config = {
  clientId: '_EV-F6ACOEWYCHLMK4U5LQ',
  clientSecret: 'IRBOFEBWNLYZZWNNNEVWKA',
  callback: 'http://localhost:3000/callback',
  baseUrl: 'https://api.zcong.me',
  authorizeUrl: '/oauth2/authorize',
  tokenUrl: '/oauth2/token',
  resourceUrl: '/oauth2/me'
}

router.get('/oauth/tictalk', ctx => {
  const params = {
    client_id: config.clientId,
    redirect_uri: config.callback,
    response_type: 'code'
  }
  // redirect to oauth server
  ctx.redirect(`${config.baseUrl}${config.authorizeUrl}?${qs.stringify(params)}`)
})

router.get('/callback', async ctx => {
  // get code from callback url
  const { code } = ctx.request.query
  if (!code) {
    console.log('bad code')
    const err = new Error('bad code')
    err.status = 400
    throw err
  }

  const data = new FormData()
  data.append('code', code)
  data.append('client_id', config.clientId)
  data.append('client_secret', config.clientSecret)
  data.append('grant_type', 'authorization_code')
  data.append('redirect_uri', config.callback)

  // post form to token api, exchange access token by code
  const resp = await axios.post(config.baseUrl + config.tokenUrl, data, {
    headers: data.getHeaders()
  })

  // now get access_token
  const {
    access_token
  } = resp.data

  if (!access_token) {
    console.log('bad access token')
    const err = new Error('bad access token')
    err.status = 400
    throw err
  }

  // use access token get user info
  const userResp = await axios({
    url: config.baseUrl + config.resourceUrl,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })

  // handle your logic here
  console.log(userResp.data)

  ctx.body = userResp.data
})

app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000, () => {
  console.log('app running at: http://localhost:3000')
})
